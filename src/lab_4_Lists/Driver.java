public class Driver {
	
	public static void main(String[] args) {
		
		boolean isPartOne = true;  // week 1 - true, week 2 - false
		
		if (isPartOne) {
			// week 1
			LinkedList<String> list = new LinkedList<String>();
			
			// addFirst() and addLast()
			list.addFirst("A");
			list.addFirst("B");
			list.addFirst("X");
			list.addFirst("Z");
			list.addLast("X");
			list.addLast("Y");
			
			// for each loop which relies on the iterator() function
			int i = 1;
			for (String key : list) {
				System.out.println(i +" : "+key);
				i++;
			}
			
			// remove in the beginning, middle and deleteAll()
			list.removeFirst();
			list.delete("Y");
			list.deleteAll("X");
			System.out.println(list);
			
		} else {
			
			// week 2
			int[] n = { 9, 18, 44, 76, 92};
			
			for (int i = 0; i < n.length; i++) {
				RomanNumeral rn = new RomanNumeral(n[i]);
				System.out.println(rn);
			}
		}
	}

}
