
public class Pair<Key, Value> {

	public Key key;
	public Value value;

	public Pair(Key key_, Value value_) {
		key = key_;
		value = value_;
	}
}
