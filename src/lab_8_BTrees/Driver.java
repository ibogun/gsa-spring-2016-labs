
public class Driver {
	public static void main(String[] args) {
		String[] strings = {"A", "X", "D","R","W", "F", "Z", "O","P", "J","K","I"};
		
		boolean isPartOne = true;
		boolean isPartTwo = true;
		System.out.println("Order = 2 * (min degree) - 1");
		if (isPartOne) {
			System.out.println("Part 1: TwoThreeFourTree (BTree of minimum degree 2, which is same as BTree of order 3)");
			System.out.println("=============================");
			TwoThreeFourTree<String> btree = new TwoThreeFourTree<String>();
			
			for (int i = 0; i < strings.length; i++) {
				System.out.println("Inserting: " + strings[i]);
				btree.insert(strings[i]);
				System.out.println("Is BTree()?: " + btree.isBTree());
				System.out.println(btree+"\n"); // not "pretty", just level-order traversal and 
				// printing nodes from left to right.
			}
		}
		
		if (isPartTwo) {
			System.out.println("Part 2: BTree (BTree of minimum degree 3, which is same as BTree of order 5)");
			System.out.println("=============================");
			BTree<String> btree = new BTree<String>(3);
			
			for (int i = 0; i < strings.length; i++) {
				System.out.println("Inserting: " + strings[i]);
				btree.insert(strings[i]);
				System.out.println("Is BTree()?: " + btree.isBTree());
				System.out.println(btree+"\n"); // not "pretty", just level-order traversal and 
				// printing nodes from left to right.
			}
		}

	}
}
