import java.util.ArrayList;

public class Driver {
	public static void main(String[] args) {
		
		int[] weights = {2, 5 , 25};
		BarbellWeights barbell = new BarbellWeights(weights);
		int weightToRepresent = 135;
		
		// Part 1: unconstrained solutions
		ArrayList<int[]> unconstrainedSolutions = 
				barbell.findAllRepresentation(weightToRepresent);
		
		System.out.println(barbell.checkCorrectnessAll(unconstrainedSolutions, 
						weightToRepresent));
		barbell.printAllSolutions(unconstrainedSolutions);
		
		// Part 2: constrained solutions
		int[] numberOfAvailableWeights = {30, 10, 3};
		ArrayList<int[]> constrainedSolutions = 
				barbell.findAllRepresentationsWithConstraint(weightToRepresent,
						numberOfAvailableWeights);
		

		System.out.println(barbell.checkCorrectnessAll(constrainedSolutions, 
				weightToRepresent));
		barbell.printAllSolutions(constrainedSolutions);
		
		
	}
}
