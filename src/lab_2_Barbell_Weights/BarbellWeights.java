import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;

public class BarbellWeights {
	
	private int[] weights;
	
	/**
	 * Constructor
	 * @param weights_ are from smallest to the largest (example: 1, 3, 10, 25)
	 */
	public BarbellWeights(int[] weights_) {

	}
	
	
	/**
	 * Find all possible representations of 'totalWeight' using array of weights 'weights'.
	 * Each representation (element of the array list), denoted as 'r',
	 * is an array of the same length as 'weights' such as:
	 * weights[0]*r[0] + ... + weights[m]*r[m] = n, where m = weights.length - 1
	 * @param totalWeight - weight to represent
	 * @return ArrayList of all possible representations
	 */
	public ArrayList<int[]> findAllRepresentation(int totalWeight){

	}
	
	
	
	/**
	 * Find all possible representations of 'n' using array of weights 'weights',
	 * subject to constraint on number of available weights.
	 * Each representation, denoted as 'r',
	 * is an array of the same length as 'weights' such as:
	 * weights[0]*r[0] + ... + weights[m]*r[m] = n, where m = weights.length - 1
	 * and r[0] <= availableWeights[0],..., r[m] <= availableWeights[m]
	 * 
	 * NOTE: it is possible to get solution by applying findAllRepresentation(int totalWeight)
	 * function and then filtering the result. It is not allowed - you have to 
	 * modify findAllRepresentation(int totalWeight) code to return solution
	 * which are subject to constraint.
	 * 
	 * @param totalWeight - weight to represent
	 * @return ArrayList of all possible representations
	 */
	public ArrayList<int[]> findAllRepresentationsWithConstraint(int n, 
			int[] availableWeights) {
	
	}

	
	/**
	 * Check if every solution's weight adds up to totalWeight
	 * @param solutions to check
	 * @param totalWeight 
	 * @return 
	 */
	public boolean checkCorrectnessAll(ArrayList<int[]> solutions, int totalWeight) {

	}

	
	/**
	 * Print all solutions
	 * @param allSolutions
	 */
	public void printAllSolutions(ArrayList<int[]> allSolutions) {

	}
}
