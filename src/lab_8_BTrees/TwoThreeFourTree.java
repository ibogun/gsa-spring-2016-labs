
public class TwoThreeFourTree<Key extends Comparable<Key>> {

	private Node<Key> root;

	public TwoThreeFourTree() {
	}


	/**
	 * Tree rooted at subtree
	 * @param subtree - root
	 */
	public TwoThreeFourTree(Node<Key> subtree) {
		root = subtree;
	}

	public Node<Key> getRoot() {
		return root;
	}

	/**
	 * Insert key into a BTree.
	 * @param key - key to be inserted
	 */
	public void insert(Key key) {
	}



	/**
	 * Search for a Key 'key' in the tree. Result should be a Pair which contains
	 * a Node with the key, and index of the key in node.keys list.
	 * @param key - key to search for
	 * @return  Pair instance with <Node, index of of the key in Node.keys>
	 */
	public Pair<Node<Key>, Integer> search(Key key) {
	}


	public String toString() {

	}

	/**
	 * Check if a given tree satisfies BTree properties
	 * @return
	 */
	public boolean isBTree() {
	
	}

}
