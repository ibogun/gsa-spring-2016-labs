
public class RomanNumeral {

	private int intRepresentation = 0;
	private LinkedList<Character> list;

	/**
	 * Convert integer into Roman numeral
	 * @param number - integer to be converted (input only in the range [1, 2000]
	 */
	public RomanNumeral(int number) {
	}
	
	public int getInt() {
		return this.intRepresentation;
	}

	public RomanNumeral next() {
	}

	public RomanNumeral previous() {
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Character i : list) {
			sb.append(i);
		}
		return new String(sb);
	}

}
