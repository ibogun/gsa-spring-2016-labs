public class Driver {

	public static void main(String[] args) {

		boolean runPartOne = true;
		boolean runPartTwo = true;

		if (runPartOne) {
			HashTable<Integer, String> table = new HashTable<Integer, String>(3);

			System.out.println("Insertion:");
			table.insert(2, "a");
			table.insert(3, "b");
			table.insert(4, "c");
			table.insert(5, "d");
			table.insert(6, "h");

			System.out.println(table);

			System.out.println("Deletion:");
			table.delete(4);
			table.delete(5);

			System.out.println(table);

			System.out.println("Lookup:");
			System.out.println(table.get(6));
			System.out.println(table.get(2));

			System.out.println("Contains:");
			System.out.println(table.contains(4));
			System.out.println(table.contains(6));

			System.out.println("Load Factor: " + table.loadFactor());

			System.out.println(table.hash(4));
			System.out.println(table.hash(2));
		}

		if (runPartTwo) {

			int M = 100000000;
			int K = 10;
			int N = 20000000;

			BloomFilter<Integer> filter = new BloomFilter<Integer>(M, K);

			for (int i = 0; i < N; i++) {
				filter.add(i);
			}

			long t1;
			long t2;

			t1 = System.currentTimeMillis();
			for (int i = 0; i < N; i++) {

				if (filter.query(i) == false) {
					System.out.println("Wrong");
				}
			}

			int nFalsePositives = 0;
			for (int i = 0; i < N; i++) {
				if (filter.query(i + N) == true)
					nFalsePositives++;
			}

			t2 = System.currentTimeMillis();

			System.out.println(
					"Time: " + (t2 - t1) / 1000 + " seconds\n" + "Number of false positives: " + nFalsePositives);

			System.out.println("Number of false positives as percentage: " + (double) nFalsePositives / N + " %");

			System.out.println(filter);
		}
	}
}
