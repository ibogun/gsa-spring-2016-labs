
public class ExternalSort {
	private int chunkSize_; // size of the chunks
	private String directory_; // directory where all the files will be saved to

	/**
	 * Sorts a file using external merge sort
	 * 
	 * @param directory
	 *            - directory of the sorted text file
	 * @param chunkSize
	 *            - how many numbers are in each text file
	 */
	public ExternalSort(String directory, int chunkSize) {
		this.directory_ = directory;
		this.chunkSize_ = chunkSize;
	}

	/**
	 * Sort array in the range	
	 * @param variableArray - array to sort
	 * @param first - range from
	 * @param last - range to
	 * @return
	 */
	public static int[] mergeSort(int[] variableArray, int first, int last) {

	}


	/**
	 * Sort inputFile and save result to the outputFile
	 * 
	 * @param inputFile
	 *            - name of the input file
	 * @param outputFile
	 *            - name of the output file
	 * @throws FileNotFoundException
	 *             - if the file is not found it will end
	 */
	public void completeSort(String inputFile, String outputFile)  {


	}



}