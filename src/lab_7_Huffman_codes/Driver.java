
import java.util.LinkedList;

public class Driver {

	public static void main(String[] args) {

		boolean isPartOne = true;
		boolean isPartTwo = true;

		if (isPartOne) {
			int n = 11;
			Heap<Integer> heap = new Heap<Integer>(n);

			for (int i = 0; i < n; i++) {
				heap.add(i);
				assert(heap.isHeap()); // compile with '-ea' option in VM
										// arguments,
				// otherwise the assertion will be ignored
				// see
				// http://stackoverflow.com/questions/5509082/eclipse-enable-assertions
			}
			System.out.println(heap);
			heap.setElementAtIndex((Integer) 500, 5); // Ruined it. Not a heap
			// anymore.
			assert (!heap.isHeap());
			System.out.println(heap);

			heap.setElementAtIndex((Integer) 8, 5); // Saved it.
			System.out.println(heap);

			while (!heap.isEmpty()) {

				heap.removeMax();
				System.out.println(heap);
			}
		}

		if (isPartTwo) {

			String str = "aaaabbbccd";

			System.out.println("Original string: " + str);
			Huffman huffman = new Huffman();
			huffman.createHuffmanTree(str);

			LinkedList<Boolean> code = huffman.encode(str);
			System.out.println(code);

			System.out.println(huffman.getHuffmanTree());
			String strDecoded = huffman.decode(code);
			System.out.println("Decoded string: " + strDecoded);

			System.out.println("Number of bits to store string: " + str.length() * 16);
			System.out.println("Number of bits to store code: " + code.size());

		}
	}

}
