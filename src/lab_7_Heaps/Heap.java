
public class Heap<Key extends Comparable<Key>> {

	private Key[] data;
	private int elements = 0;

	@SuppressWarnings("unchecked")
	public Heap(int capacity) {
		this.data = (Key[]) new Comparable[capacity + 1];
	}

	/**
	 * Add element to the heap.
	 * 
	 * @param key
	 */
	public void add(Key key) {

	}

	/**
	 * Remove maximum element from the heap.
	 * 
	 * @return max element
	 */
	public Key removeMax() {

	}

	// Testing function. Do not edit.
	public Key getElementAtIndex(int i) {
		return this.data[i];
	}

	// Testing function. Do not edit.
	public void setElementAtIndex(Key key, int i) {
		this.data[i] = key;
	}

	/**
	 * Add element at specific location while maintaining heap property.
	 * 
	 * @param key
	 *            - key to be added
	 * @param i
	 *            - index
	 */
	public void setElementAtIndexBalanced(Key key, int i) {

	}

	/**
	 * Check if heap satisfies max-heap property.
	 * 
	 * @return
	 */
	public boolean isHeap() {
	}

	/**
	 * Tree-like representation of the heap. You may optimize this function for
	 * the heap with no more than 15 elements.
	 */
	public String toString() {

	}

	public boolean isEmpty() {
		// return true if the heap is empty, false otherwise
	}

	public int size() {
		// return number of added elements
	}

}
