
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Driver {
	public static void testIfSorted(String input) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(input));

		int previous = scanner.nextInt();
		int next = 0;

		int totalIncorrect = 0;

		int countIntegers = 0;

		while (scanner.hasNext()) {
			countIntegers++;
			next = scanner.nextInt();

			if (next < previous) {
				totalIncorrect++;
			}

			previous = next;
		}

		if (totalIncorrect == 0) {
			System.out.println("Sorting is correct!");
		} else {
			System.out.println("Total incorrect " + totalIncorrect);
		}

		System.out.println("Number of elements: " + countIntegers);
		scanner.close();
	}

	public static void main(String[] args) throws IOException {
		
		// directory where temporary files will be saved to. 
		// (all temporary files should be deleted after  execution)
		// Note: Paths are written differently for different operating systems.
		// This won't be a problem as your program will be run during grading on the OS
		// where the path is written according to the OS's rules.
		String tmpDirectory = "/Users/Ivan/Downloads/ExternalSort/tmp/"; 
		
		int chunkSize = 100;
		
		ExternalSort externalsort = new ExternalSort(tmpDirectory, chunkSize);

		// path to the unsorted file
		String inputFileName = "/Users/Ivan/Downloads/ExternalSort/unsorted_small.txt";
		// file which will be created
		String outputFileName = "/Users/Ivan/Downloads/ExternalSort/sorted_small.txt";

		long t1 = System.currentTimeMillis();
		externalsort.completeSort(inputFileName, outputFileName);
		long t2 = System.currentTimeMillis();
		System.out.println("time: " +(t2-t1)/1000);
		try {
			testIfSorted(outputFileName);
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
