
public class BloomFilter<Key> {

	private final int BIG_PRIME = 179425033; // big prime number
	private int M; // total number of elements
	private int N = 0; // number of currently added elements
	private int K; // number of hash functions

	private boolean[] bitArray;

	public BloomFilter(int M_, int K_) {
		this.M = M_;
		this.K = K_;
		this.bitArray = new boolean[M_];
	}

	/**
	 * Add the key
	 * 
	 * @param key
	 *            - key to be added
	 */
	public void add(Key key) {
	}

	/**
	 * Check if the key is present. Note that Bloom filter guarantees that if
	 * the key is not present then query(key) = false, the opposite is not true,
	 * however. If the query(key) = true -> it might be false positive ( the key
	 * might not actually be present)
	 * 
	 * @param key
	 *            to check
	 * @return true if the element is present, false otherwise
	 */
	public boolean query(Key key) {
	}

	private int getKeyHash(Key key) {
		return key.hashCode();
	}

	/**
	 * Use a set of hash functions in the form:
	 * 
	 * h_ab(k) = ((a*k + b) mod BIG_PRIME) mod M) where a = 1 + hashedTimes 
	 * b = hashedTimes*(1/BIG_PRIME)
	 * where hashedTimes is the integer from the set [0,...,k-1]
	 * 
	 * @param k
	 *            result of the java's hashCode() of the original Key
	 * @param hashedTimes
	 *            number from 0 - (K-1)
	 * @return
	 */
	private int hash(int k, int hashedTimes) {
	}

	
	/**
	 * Mod function which works even when x is negative
	 * @param x 
	 * @param y
	 * @return
	 */
	private int mod(int x, int y) {
		int result = x % y;
		if (result < 0) {
			result += y;
		}
		return result;
	}

	public String toString() {
		// String representation
	}


}
