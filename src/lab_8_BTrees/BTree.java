public class BTree<Key extends Comparable<Key>> extends TwoThreeFourTree<Key> {

	/**
	 * Construct a BTree of minimum degree 'minDegree'. 
	 * 
	 * @param order
	 */
	public BTree(int order) {

	}

	public BTree() {
		super();
	}

	public BTree(Node<Key> subtree) {
	}

	/**
	 * Search for a Key 'key' in the tree. Result should be a Pair which contains
	 * a Node with the key, and index of the key in node.keys list.
	 * @param key - key to search for
	 * @return  Pair instance with <Node, index of of the key in Node.keys>
	 */
	public Pair<Node<Key>, Integer> search(Key key) {
	}

	/**
	 * Insert key into a BTree.
	 * @param key - key to be inserted
	 */
	public void insert(Key key) {
	}
	
	public boolean isBTree(){
	}
}
