public class Heap<Key extends Comparable<Key>> {

	private Key[] data;
	private int elements = 0;

	@SuppressWarnings("unchecked")
	public Heap(int capacity) {
		this.data = (Key[]) new Comparable[capacity + 1]; // workaround to 
		// create array of generics
	}


	/**
	 * Add element to the heap
	 * @param key - element to be added
	 */
	public void add(Key key) {
		
	}

	/**
	 * Remove maximum element from the heap.
	 * @return Maximum element in the heap
	 */
	public Key removeMax() {
	}

	// For testing purposes. Do not edit.
	public Key getElementAtIndex(int i) {
		return this.data[i];
	}

	// For testing purposes. Do not edit.	
	public void setElementAtIndex(Key key, int i) {
		this.data[i] = key;
	}

	/**
	 * Check if current heap satisfies heap property.
	 * @return true if it is a heap, false otherwise
	 */
	public boolean isHeap() {
	
	}


	/**
	 * Tree-like representation of the heap. You may optimize this function for
	 * the heap with no more than 15 elements.
	 */
	public String toString() {
	
	}


	public boolean isEmpty() {
		return elements == 0;
	}


}
