
public class Huffman {

	/**
	 * Create Huffman tree given characters of a string.
	 * @param str - String to use when building Huffman tree
	 */
	public void createHuffmanTree(String str) {
	
	}

	/**
	 * Encode a message using Huffman tree. Note, that Huffman tree is not unique,
	 * so testing will be performed on the size of the resulting LinkedList and 
	 * whether that string can be decoded back to the original message
	 * @param message - string to be encoded
	 * @return - huffman code
	 */
	public LinkedList<Boolean> encode(String message){

	}
	
	
	/**
	 * Decode code given Huffman tree.
	 * @param code - huffman code to be decoded
	 * @return - original string
	 */
	public String decode(LinkedList<Boolean> code){
	}
	
	/**
	 * Print out Huffman tree using level-order traversal.
	 */
	public String toString(){
	}

}