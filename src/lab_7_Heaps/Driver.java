
import java.util.Random;

public class Driver {

	public static void main(String[] args) {
		int n = 11;
		Heap<Integer> heap = new Heap<Integer>(n);

		for (int i = 0; i < n; i++) {
			heap.add(i);
			assert (heap.isHeap()); // compile with '-ea' option in VM
									// arguments,
			// otherwise the assertion will be ignored
			// see
			// http://stackoverflow.com/questions/5509082/eclipse-enable-assertions
		}
		System.out.println(heap);
		heap.setElementAtIndex(500, 5); // Ruined it. Not a heap
		// anymore.
		assert (!heap.isHeap());
		System.out.println(heap);

		heap.setElementAtIndex(8, 5); // Saved it.
		System.out.println(heap);

		heap.setElementAtIndexBalanced(15, 5); // Set element while maintaining
		// heap property
		System.out.println(heap);

		// delete elements one by one.
		while (!heap.isEmpty()) {
			heap.removeMax();
			System.out.println(heap);
		}

		// Priority queue
		Random rnd = new Random(0);  // set seed to 0 for deterministic results
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(n);

		for (int i = 0; i < n; i++) {
			Integer r = rnd.nextInt(100);
			pq.enqueue(r);
		}

		System.out.println("Max element: " + pq.remove());

		assert (pq.size() == n - 1);

		System.out.println();
		for (Integer integer : pq) {
			System.out.print(integer + " ");
		}

	}

}
