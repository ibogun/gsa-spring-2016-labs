import java.util.List;

public class Node<Key extends Comparable<Key>> {

	// Although, these fields should be private according to the java style,
	// leave them public for debugging and testing purposes.
	public int n = 0;   			// number of keys in the node
	public boolean isLeaf = true;   
	public List<Key> keys;			// list of keys in the node
	public List<Node<Key>> children;// list of children

	public Node() {
		// constructor
	}

	public int getN() {
		return keys.size();
	}

	public void addKey(Key key) {
		keys.add(key);
		n++;
	}

	public List<Key> getKeys() {
		return keys;
	}

	public List<Node<Key>> getChildren() {
		return children;
	}

	public boolean isLeaf() {
		return isLeaf;
	}

	public int numberOfKeys() {
		return n;
	}


	public String toString() {

	}

	/**
	 * Check if a tree rooted at 'this' node is a BTree.
	 * @return true if BTree, false otherwise
	 */
	public boolean isBTreeNode() {

	}
}
