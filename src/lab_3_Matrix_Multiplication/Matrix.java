
public class Matrix {
	
	
	private double[][] array;  // array with elements
	private int n;		       // number of columns, rows

	
	// %%%%%%%%%%%%%%%%%%%%%%%%%%    PART 1    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	/**
	 * Initialize matrix from the array
	 * @param array_ - array
	 */
	public Matrix(double[][] array_) {
	}


	/**
	 * Initialize the matrix where the number of columns and rows is n
	 * @param n_  - number of columns and rows 
	 */
	public Matrix(int n_) {
	}

	
	/**
	 * Perform regular matrix multiplication
	 * @param b  - matrix to multiply
	 * @return   matrix which is a product
	 */
	public Matrix multiply(Matrix b) {
	}


	/**
	 * Function to compare two matrices (matrix 'this' and 'b'). Two matrices are
	 * said to be equal if they
	 * 1) have same size
	 * 2) absolute value of the difference between corresponding elements is not 
	 * more than precision (you can set precision to 0.0000001).
	 * @param b - matrix to use for comparison
	 * @return true if matrix are equivalent, false otherwise
	 */
	public boolean equals(Matrix b) {
	}
	
	/**
	 * Add two matrices
	 * @param b  - matrix to be added
	 * @return   - sum
	 */
	public Matrix add(Matrix b) {
		
	}

	
	/**
	 * Return string representing the matrix. 
	 */
	public String toString() {

	}
	
	// %%%%%%%%%%%%%%%%%%%%%%%%%% END OF PART 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	// %%%%%%%%%%%%%%%%%%%%%%%%%%    PART 2    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	/**
	 * Perform matrix multiplication using Strassen algorithm
	 * @param b  - matrix to multiply
	 * @return   matrix which is a product
	 */
	public Matrix multiplyStrassen(Matrix b) {
		return null;
	}

	// %%%%%%%%%%%%%%%%%%%%%%%%%% END OF PART 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}



