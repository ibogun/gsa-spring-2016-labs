public class HashTable<Key, Value> {

    private int N;   // number of elements currently in the table
    private int M;   // number of lists
    private LinkedList<Key, Value>[] lists;

   
	public HashTable(int M) {
    
    }

    public int hash(Key key) {
    	// Hash function which maps Key -> [0,...,lists.length-1]
    }

    public Value get(Key key) {
    	// return value given key
    }
    
    public boolean contains(Key key){
    	// check if the key is present in the table
    }

    public void insert(Key key, Value val) {
        // insert key-value pair into the table
    }

    public void delete(Key key) {
        // delete key from the table
    }
    
    public double loadFactor() {
    	// calculate load factor of the table
    }
    
    public String toString() {
      // String representation of the table
    }

}
