

public class PriorityQueue<Key extends Comparable<Key>> implements Iterable<Key> {

	private Heap<Key> heap;

	public PriorityQueue(int capacity) {
		// constructor
	}

	public void enqueue(Key key) {
		// add to the queue
	}

	public Key remove() {
		// remove maximum element
	}

	public boolean isEmpty() {
		// check if queue is empty
	}

	public int size() {
		// return size of the queue (number of added elements
	}

	public Iterator<Key> iterator() {
		// iterator over the queue. Should iterate over largest to smallest
	}

}
