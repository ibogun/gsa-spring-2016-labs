
import java.util.Random;

public class Driver {

	public static void main(String[] args) {
		
		// generate a power of two and randomly create two matrices
		int n=(int) Math.pow(2, 2);
		Random rnd = new Random(2016);
		
		double[][] el_1=new double[n][n];
		double[][] el_2=new double[n][n];
		
		for (int i = 0; i < el_2.length; i++) {
			for (int j = 0; j < el_2.length; j++) {
				el_1[i][j]=rnd.nextDouble();
				el_2[i][j]=rnd.nextDouble();
			}
		}
		
		
		Matrix m1 = new Matrix(el_1);
		Matrix m2 = new Matrix(el_2);
		
		System.out.println("Matrices to be multiplied: ");
		System.out.println(m1);
		System.out.println(m2);
		
		Matrix productMultiply=m1.multiply(m2);

		System.out.println("Regular matrix multiply result:");
		System.out.println(productMultiply);
		
		Matrix productStrassenMultiply=m1.multiplyStrassen(m2);
		
		System.out.println("Strassen matrix multiply result:");
		System.out.println(productStrassenMultiply);
		
		// print if matrices are the same
		System.out.println("Are product the same? " +productMultiply.equals(productStrassenMultiply));
		
	}

}

