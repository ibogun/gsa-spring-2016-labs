
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<Key> implements Iterable<Key> {


	private class Node {

		private Key key;
		private Node next;

	}

	private Node firstNode;
	private Node lastNode;
	private int size;

	public Node getFirstNode() {
	}

	
	/**
	 * Return the first element in the list
	 * @return - first key
	 */
	public Key getFirst() {
	}
	
	
	/**
	 * Return the last element in the list
	 * @return - last key
	 */
	public Key getLastKey() {
	}
	

	
	/**
	 * Check if the list contains a key
	 * @param k - key to search
	 * @return
	 */
	public boolean contains(Key k) {
	}
	

	
	/**
	 * Insert in the beginning
	 * @param k - key to insert
	 */
	public void addFirst(Key k) {
	}

	/**
	 * Insert in the end
	 * @param k - key to insert
	 */
	public void addLast(Key key) {
	}
	
	public boolean isEmpty() {
	}
	
	
	/**
	 * Delete all occurrences of the key
	 * @param k - key to delete
	 */
	public void deleteAll(Key k) {
	}
	
	/**
	 * Delete the key
	 * @param k - key to delete
	 */
	public void delete(Key k) {
	}
	
	/**
	 * Remove the first element of the list 
	 * @return removed element
	 */
	public Key removeFirst(){
	}

 
	/**
	 * Iterator over the list
	 */
	public Iterator<Key> iterator() {
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[ ");
		for (Key k : this) {
			sb.append(k.toString()+ " ");
		}
		sb.append("]");
		return new String(sb);
	}

}
